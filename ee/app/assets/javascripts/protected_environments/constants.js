export const ACCESS_LEVELS = {
  DEPLOY: 'deploy_access_levels',
};

export const LEVEL_TYPES = {
  ROLE: 'role',
  USER: 'user',
  GROUP: 'group',
};
